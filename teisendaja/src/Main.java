import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Main extends Application{
    private BorderPane layout;
    private Scene scene;

    public static void main(String[] args) {
        launch(args);
    }
    
    
    @Override
    public void start(Stage window) throws Exception {
        layout = new BorderPane();

        //loome klassi andmetele, see avab Andmed.xml faili ning selle abil saab failis olevaid andmeid t66tleda
        Andmed andmed = new Andmed();
        String[] yhikud = andmed.getYhikud();

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);

        TextField algPikkus = new TextField();
        GridPane.setConstraints(algPikkus, 0, 0);
        algPikkus.setPromptText("Algpikkus");

        TextField l6ppVastus = new TextField();
        GridPane.setConstraints(l6ppVastus, 1, 0);
        l6ppVastus.setPromptText("Vastus");

        ChoiceBox<String> algAndmed = new ChoiceBox<>();
        algAndmed.setTooltip(new Tooltip("Vali millest soovid teisendada "));
        for(int i=0;i<yhikud.length;i++){
            algAndmed.getItems().add(yhikud[i]);
        }

        GridPane.setConstraints(algAndmed, 0, 1);

        ChoiceBox<String> l6ppAndmed = new ChoiceBox<>();
        for(int i=0;i<yhikud.length;i++){
            l6ppAndmed.getItems().add(yhikud[i]);
        }

        GridPane.setConstraints(l6ppAndmed, 1, 1);
        l6ppAndmed.setTooltip(new Tooltip("Vali milleks soovid teisendada"));
        l6ppAndmed.setValue("meeter");
        Button nupp = new Button("Arvuta");
        GridPane.setConstraints(nupp, 1, 2);

        nupp.setOnAction(e -> {
            double algneV22rtus = Double.valueOf(algPikkus.getText());
            String algneYhik = algAndmed.getValue().toString();
            String l6ppYhik = l6ppAndmed.getValue().toString();

            Arvuta arvutaja = new Arvuta(andmed);
            arvutaja.setAndmed(algneV22rtus, algneYhik, l6ppYhik);
            l6ppVastus.setText(arvutaja.getVastus()+""+arvutaja.getYhikud()[1]);
        });

        grid.getChildren().addAll( nupp, algPikkus, l6ppVastus, algAndmed, l6ppAndmed);

        scene = new Scene(grid, 450, 300);
        window.setResizable(false);
        window.setTitle("Distantsi teisendaja");
        window.setScene(scene);
        window.show();

    }

}
