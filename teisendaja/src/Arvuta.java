/*Klass lõppvastuse arvutamiseks.
*  Andmed.xml failist saadud andmete järgi võetakse ühikute suhted meetriga
*  Algne pikkus teisendatakse meetritesse
*  Arvutatakse meetri suhe lõppühikuga
*  Teisendused korrutatakse omavahel, ning vastus on olemas.
* */
public class Arvuta {

    //ette antud
    public double algne;
    public String algYhik;
    public String l6ppYhik;
    //arvutada
    public double meetrites;
    public double vastus;
    //andmed xml failist
    public Andmed andmedK6ik;
    public String[] l6ppAndmed;
    public String[] algAndmed;
    
    

    public Arvuta(Andmed andmed){
        //andmed xml failist antakse Main klassist ette
        this.andmedK6ik = andmed;
    }
    /*Meetod mis määrab muutujates vajalikud andmed,
     * võtab käesolevate ühikute kohta andmed,
     * ning käivitab meetodid arvutaMeetrid ning arvutaVastus
     * */
    public void setAndmed(double a, String b, String c) {
        if (a >= 0) {
        	this.algne = a;
        }else{
        	this.algne = a * -1;
        }
        this.algYhik = b;
        this.l6ppYhik = c;
        //v6tame yhikute kohta andmed
        this.l6ppAndmed = andmedK6ik.getAndmedByName(l6ppYhik);
        this.algAndmed = andmedK6ik.getAndmedByName(algYhik);
        arvutaMeetrid();
        arvutaVastus();
        
    }
/*       meetod arvutab algpikkuse meetritese
 * algpikkuse selle suhtega. Tulemuseks on vastus. */
 
    public void arvutaMeetrid(){
        double dbl = Double.parseDouble(this.algAndmed[1]);
        this.meetrites = dbl * algne;
    }
//  	meetod arvutab lõppühiku suhte meetriga ning korrutab 
    public void arvutaVastus(){
        double dbl = Double.parseDouble(this.l6ppAndmed[1]);
        double kordaja = 1 / dbl;
        this.vastus = kordaja*this.meetrites;

        System.out.println(algne+" "+algAndmed[2]+" on "+vastus+" "+l6ppAndmed[2]);
    }

    public double getVastus() {
        return vastus;
    }

    public String[] getYhikud(){
        return new String[]{algAndmed[2], l6ppAndmed[2]};
    }
}
