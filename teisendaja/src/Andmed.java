import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;

public class Andmed {
    public NodeList andmed;
    public String[] yhikud;

    public Andmed(){
        avaAndmed();
        getYhikudFromXML();
    }

    public void avaAndmed(){
        try{
        	
            File dataFile = new File("./src/Andmed.xml");
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document dbFileData = docBuilder.parse(dataFile);

            NodeList data = dbFileData.getElementsByTagName("yhik");
            this.andmed = data;
        }catch (Exception e){
            System.out.println("Data file could not be loaded.");
        }
    }

    public NodeList getAndmed() {
        return this.andmed;
    }

    public String[] getAndmedByName(String nimi){
        String[] andmed = new String[3];
        for(int i=0; i<this.andmed.getLength(); i++){
            Element ajutine = (Element) this.andmed.item(i);
            String ajutineNimi = ajutine.getElementsByTagName("nimi").item(0).getTextContent();
            if(ajutineNimi.equals(nimi)){
                andmed[0] = ajutine.getElementsByTagName("nimi").item(0).getTextContent();
                andmed[1] = ajutine.getElementsByTagName("meetrites").item(0).getTextContent();
                andmed[2] = ajutine.getElementsByTagName("t2his").item(0).getTextContent();
            }
        }
        return andmed;
    }

    public void getYhikudFromXML(){
        this.yhikud = new String[ this.andmed.getLength() ];
        for(int i=0; i<this.andmed.getLength();i++){
            Element ajutine = (Element) this.andmed.item(i);
            String ajutineNimi = ajutine.getElementsByTagName("nimi").item(0).getTextContent();
            this.yhikud[i] = ajutineNimi;
        }
    }

    public String[] getYhikud() {
        return yhikud;
    }
}



